function layThongTinTuForm() {
    var id = document.getElementById('ID').value;
    var taiKhoan = document.getElementById('taiKhoan').value;
    var hoTen = document.getElementById('hoTen').value;
    var matKhau = document.getElementById('matKhau').value;
    var email = document.getElementById('email').value;
    var ngonNgu = document.getElementById('ngonNgu').value;
    var moTa = document.getElementById('moTa').value;
    var hinhAnh = document.getElementById('hinhAnh').value;
    var loaiND = document.getElementById('loaiND').value;

    console.log({ id, taiKhoan, hoTen, matKhau, email, ngonNgu, moTa, hinhAnh, loaiND });
    
    var user = {
        id: id, 
        taiKhoan: taiKhoan, 
        hoTen: hoTen, 
        matKhau: matKhau, 
        email: email,
        ngonNgu: ngonNgu, 
        moTa: moTa, 
        hinhAnh: hinhAnh, 
        loaiND: loaiND,
    };
    return user;
}

function convertString(maxLength, value){
    if(value.length > maxLength){
        return value.slice(0, maxLength) + "..."
    }else{
        return value;
    }

}

function showTongTinLenForm(res) {
    document.getElementById('ID').value = res.id;
    document.getElementById('taiKhoan').value = res.taiKhoan;
    document.getElementById('hoTen').value = res.hoTen;
    document.getElementById('matKhau').value = res.matKhau;
    document.getElementById('email').value = res.email;
    document.getElementById('ngonNgu').value = res.ngonNgu;
    document.getElementById('moTa').value = res.moTa;
    document.getElementById('hinhAnh').value = res.hinhAnh;
    document.getElementById('loaiND').value = res.loaiND;
}