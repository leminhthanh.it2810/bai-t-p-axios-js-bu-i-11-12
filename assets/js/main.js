const BASE_URL = "https://63b2c9a45901da0ab36dbbe4.mockapi.io";
function renderUserList(user) {
    var contentHTML = "";
    user.forEach(function (item) {
        var contentTr = `<tr>
                            <td>${item.id}</td>
                            <td>${item.taiKhoan}</td>
                            <td>${item.hoTen}</td>
                            <td>${item.matKhau}</td>
                            <td>${item.email}</td>
                            <td>${item.ngonNgu}</td>
                            <td>${item.moTa}</td>
                            <td>${convertString(50, item.hinhAnh)}</td>
                            <td>${item.loaiND}</td>
                            <td>
                                <button onclick="xoaNguoiDung('${item.id}')" class="btn btn-danger">Xóa</button>
                                <button onclick="suaNguoiDung('${item.id}')" class="btn btn-warning">Sửa</button>                          
                            </td>
                        </tr>`;
        contentHTML += contentTr;
    });
    document.getElementById("tbodynguoidung").innerHTML = contentHTML;
}

function fetchUserList() {
    axios({
        url: `${BASE_URL}/user`,
        method: "GET",
    })
        .then(function (res) {
            var userlist = res.data;
                renderUserList(userlist);
        }).catch(function (err) {
            console.log("🚀 ~ file: main.js:40 ~ err", err)
        })
}

fetchUserList();

function xoaNguoiDung(id) {
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            fetchUserList();
        })
        .catch(function (err) {
            console.log("🚀 ~ file: main.js:49 ~ xoaNguoiDung ~ err", err)
        })
}

function themNguoiDung() {
    var user = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/user`,
        method: "POST",
        data: user,
    }).then(function (res) {
        fetchUserList();
    }).catch(function (err) {
    console.log("🚀 ~ file: main.js:62 ~ themNguoiDung ~ err", err)
    })
}
// pending, resolve (success), reject (fail)

function suaNguoiDung(id) {
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "GET",
    }).then(function(res){
       console.log("🚀 ~ file: main.js:72 ~ suaNguoiDung ~ res", res);
       showTongTinLenForm(res.data);
    }).catch(function(err){
       console.log("🚀 ~ file: main.js:75 ~ suaNguoiDung ~ err", err)
    })
}

function capNhatNguoiDung(id){
    var userSaukhisua = layThongTinTuForm();
    var id = userSaukhisua.id;
    axios({
        url: `${BASE_URL}/user/${id}`,
        method: "PUT",
        data: userSaukhisua,
    }).then((res) => {
            console.log(res);
            fetchUserList();
          })
          .catch((err) => {
           console.log(err);
          });
}